package com.citi.Trade.Dto;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

import org.junit.Test;

import com.citi.TradeDto.Product;

public class ProductTest {

	@Test
	public void testProductInstanceIsNotNull() {
		assertThat(new Product(1,"Apple",1.99), is(not(nullValue())));
	}

}
